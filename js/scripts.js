$(document).ready(function() {

   $('select').material_select();

   // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
   $('.modal-trigger').leanModal({
      complete: function() {$("#selected-range").children("span").remove();} // Callback for Modal close
    });


   var rangeSlider = document.getElementById('confidence');

 noUiSlider.create(rangeSlider, {
        start: [90, 100],
        connect: true,
        step: 1,
        range: {
            'min': 0,
            'max': 100
        },
        format: wNumb({
            decimals: 0
        })
 });

 $('.button-collapse').sideNav({
    menuWidth: 300, // Default is 240
    edge: 'right', // Choose the horizontal origin
    closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
  }
);

$('.subscribe').click(function(){
    if($(this).hasClass("app-blue-text")){
        $(this).removeClass("app-blue-text");
    }
    else{
        $(this).addClass("app-blue-text")
    }
});


    $(".blackline").click(function(){
        var  conf_range = rangeSlider.noUiSlider.get();
        // console.log(conf_range[0]);
        // console.log(conf_range[1]);
        $("#selected-range").append("<span class='top4-range'> (" + conf_range[0] + "% -  " + conf_range[1]+ "%)</span>");
    });

    $("#top150comp").hide();

    $(".top150").click(function(){
        $("#selection").hide();
        $("#top150comp").show();
    });

 });
